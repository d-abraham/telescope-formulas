function focalRatio(fl,apt) {
  return Math.round((fl/apt)*20)/20;
}
function magnification(tfl,epfl){
  return Math.round((tfl/epfl)*20)/20;
}
function fieldOfView(afov,mag){
  return Math.round((afov/mag)*20)/20;
}
function exitpupil(apt,mag){
  return Math.round((apt/mag)*20)/20;
}
function maxMag(apt){
  return Math.round((apt*2.5)*20)/20
}
function eyepiecePower(tfl,mag){
  return Math.round(tfl/mag);
}
document.getElementById('fr-btn').addEventListener("click",() =>{
  document.getElementById('FR-output').innerHTML = `= ${focalRatio(document.getElementById('f-langth').value,document.getElementById('aperture').value)}`;
});
document.getElementById('mag-btn').addEventListener("click",() =>{
  document.getElementById('mag-output').innerHTML = `= ${magnification(document.getElementById('TFL').value,document.getElementById('EPFL').value)}`;
});
document.getElementById('fov-btn').addEventListener("click",() =>{
  document.getElementById('fov-output').innerHTML = `= ${fieldOfView(document.getElementById('afov').value,document.getElementById('mag').value)}`;
});
document.getElementById('exitPupil-btn').addEventListener("click",() =>{
  document.getElementById('exitPupil-output').innerHTML = `= ${exitpupil(document.getElementById('apt').value,document.getElementById('mag2').value)}`;
});
document.getElementById('maxMag-btn').addEventListener("click",() =>{
  document.getElementById('maxMag-output').innerHTML = `= ${maxMag(document.getElementById('apt2').value)}`;
});
document.getElementById('epPower-btn').addEventListener("click",() =>{
  document.getElementById('epPower-output').innerHTML = `= ${eyepiecePower(document.getElementById('tfl2').value,document.getElementById('mag3').value)}`;
});
